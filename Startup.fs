namespace web

open System
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open WebSharper.AspNetCore
open Microsoft.AspNetCore.Authorization
// for AddDefaultTokenProviders
open Microsoft.AspNetCore.Identity;

open Microsoft.AspNetCore.Builder.Extensions

open Microsoft.AspNetCore.Http.Abstractions

type Startup() =

    member this.ConfigureServices(services: IServiceCollection) =
        services.AddHttpContextAccessor()|>ignore
        // add user store as singleton so our basic implementation works. If we made it transient here
        // we would get a new empty store every request.
        services.AddSingleton<IUserStore<ApplicationUser>, UserStore>() |> ignore
        // The default AuthorizationService will go over all AuthorizationHandlers and
        // check they succeed: https://docs.microsoft.com/en-us/aspnet/core/security/authorization/policies?view=aspnetcore-5.0#iauthorizationservice
        // It suffices then to inject our authorization handler
        //services.AddSingleton<IAuthorizationHandler, ApplicationAuthorizationHandler>() |> ignore

        services.AddIdentityCore<ApplicationUser>( fun options ->
                                                       options.User.RequireUniqueEmail <- true
                                                       options.SignIn.RequireConfirmedEmail <- true)
            .AddDefaultTokenProviders()
            // need to add signInManager as AddIdentityCore doesn't do it itself
            .AddSignInManager<SignInManager<ApplicationUser>>()
            // needed to create a user (and setting the pasword hash value)
            .AddUserManager<UserManager<ApplicationUser>>()
            // this doesn't seem to inject the same way as AddScoped use above, so commented
            //.AddClaimsPrincipalFactory<ApplicationUserClaimsPrincipalFactory<ApplicationUser>>()
            |> ignore

        services.AddAuthorization(
            fun options ->
                                    // options.AddPolicy will make a policy available
                                    options.AddPolicy("Level2",System.Action<AuthorizationPolicyBuilder>(fun policy -> policy.RequireClaim("level","2")|>ignore))
                                    options.AddPolicy("Authenticate",System.Action<AuthorizationPolicyBuilder>(fun policy -> policy.RequireAuthenticatedUser()|>ignore))
                                    ()
            )
        |> ignore


        services.AddSitelet(Site.Main)
            .AddAuthentication("Identity.Application")
            .AddCookie("Identity.Application", fun options ->
                                options.LoginPath <- PathString("/login")
                                options.AccessDeniedPath <- PathString("/forbidden/")
            )
            // for signing out with signin manager
            // https://github.com/aspnet/Identity/issues/2082
            // to avoid, we should be able to call signout on httpcontext, but doesn't seem to be available
            .AddCookie("Identity.External", fun options -> ())
            .AddCookie("Identity.TwoFactorUserId", fun options -> ())
            .AddCookie("WebSharper")
        |> ignore
        //services.AddAuthentication().AddCookie("Identity.Application")

    member this.Configure(app: IApplicationBuilder, env: IWebHostEnvironment) =
        app.UseDeveloperExceptionPage() |> ignore

        app.UseStaticFiles()
            .UseWebSharperEndpointRouting<EndPoint>()
            .Run(fun context ->
                context.Response.StatusCode <- 404
                context.Response.WriteAsync("Page not found"))
        //printfn "********************************************************************************"
        //// this does work in app run, but not in test
        //printfn "in Configure, signin manager is %A" (app.ApplicationServices.GetService(typeof<SignInManager<ApplicationUser>>))
        //// this works in tests:
        //let scope = app.ApplicationServices.CreateScope()
        //printfn "in Configure, signin manager is %A" (scope.ServiceProvider.GetService(typeof<SignInManager<ApplicationUser>>))
        //printfn "********************************************************************************"

module Program =

    let WebHostBuilder args =
        WebHost
            .CreateDefaultBuilder(args)
            .UseStartup<Startup>()

    let BuildWebHost args =
        let builder = WebHostBuilder args
        builder.Build()

    // added for tests. It failed as it was missing this function.
    // I don't know why it calls this, as I don't put any reference to Program, only to Startup...
    let CreateWebHostBuilder args =
        let builder = WebHost.CreateDefaultBuilder(args)
        builder.UseStartup<Startup>()

    [<EntryPoint>]
    let main args =
        BuildWebHost(args).Run()
        0
