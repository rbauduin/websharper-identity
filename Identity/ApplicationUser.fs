namespace web
open Microsoft.AspNetCore.Identity
open System.Security.Claims
open FSharp.Core

// if you want to return null in a Task<ApplicationUser> as needed
// for the user store, you need to add this annotation.
[<AllowNullLiteral>]
type ApplicationUser() =
    inherit IdentityUser()
    let mutable _claims:Claim seq = Seq.empty
    member self.claims
        with get() = _claims
        and set(c) = _claims <- c
    member self.addClaims(c) = _claims <- (Seq.append _claims c)
