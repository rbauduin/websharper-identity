namespace web
open System;
open System.Collections.Generic;
open System.Data.SqlClient;
open System.Linq;
open System.Threading;
open System.Threading.Tasks;
open Microsoft.AspNetCore.Identity;
open Microsoft.Extensions.Configuration;
open System.Security.Claims

// Based on example at https://markjohnson.io/articles/asp-net-core-identity-without-entity-framework/
type UserStore()=
    let users : ApplicationUser list ref = ref []
    member this.FilterByAsync f cancellationToken=
        async {
        let filtered= !users |> List.filter f
        if List.length filtered = 1 then
                let found = filtered |> List.head
                return found
        else
                return null
        } |> fun a -> Async.StartAsTask (a,cancellationToken=cancellationToken)
    // I would have liked to populate the list here, but I need the UserManager instance to
    // set the password, and I didn't find a way to achieve that.
    interface IUserStore<ApplicationUser> with
        member this.GetUserIdAsync(user:ApplicationUser, cancellationToken:CancellationToken) =
            async {
                return user.Id
            } |> fun a -> Async.StartAsTask (a,cancellationToken=cancellationToken)
        member this.GetUserNameAsync(user:ApplicationUser, cancellationToken:CancellationToken) =
            async {
                return user.UserName
            } |> fun a -> Async.StartAsTask (a,cancellationToken=cancellationToken)
        member this.SetUserNameAsync(user:ApplicationUser, userName:string, cancellationToken:CancellationToken) =
            user.UserName <- userName
            // FIXME: is this correct?
            Task.CompletedTask
        member this.GetNormalizedUserNameAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<string> =
            Task.FromResult(user.NormalizedUserName)
        member this.SetNormalizedUserNameAsync(user: ApplicationUser, normalizedName: string, cancellationToken: CancellationToken) : Task =
            user.NormalizedUserName <- normalizedName
            Task.CompletedTask
        member this.CreateAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<IdentityResult> =
            // create user in store
            users := user :: !users
            Task.FromResult(IdentityResult.Success)
        member this.UpdateAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<IdentityResult> =
            // update user in store
            // ......
            Task.FromResult(IdentityResult.Success)
        member this.DeleteAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<IdentityResult> =
            // delete user in store
            // ......
            Task.FromResult(IdentityResult.Success)
        member this.FindByIdAsync(userId: string, cancellationToken: CancellationToken) : Task<ApplicationUser> =
            this.FilterByAsync (fun i -> i.Id=userId) cancellationToken
        member this.FindByNameAsync(normalizedUserName: string, cancellationToken: CancellationToken) : Task<ApplicationUser> =
            this.FilterByAsync (fun i -> i.NormalizedUserName=normalizedUserName) cancellationToken
        member this.Dispose(): unit =
            ()
    interface IUserPasswordStore<ApplicationUser> with
        member this.SetPasswordHashAsync(user: ApplicationUser, passwordHash: string , cancellationToken: CancellationToken):  Task =
            user.PasswordHash <- passwordHash
            Task.CompletedTask
        member this.GetPasswordHashAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<string> =
            Task.FromResult(user.PasswordHash)
        member this.HasPasswordAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<bool> =
            Task.FromResult(not (isNull user.PasswordHash))

    interface IUserClaimStore<ApplicationUser> with
        member this.AddClaimsAsync(user:ApplicationUser, claims: IEnumerable<Claim>, cancellationToken:CancellationToken) =
            let existing = user
            user.addClaims claims
            Task.CompletedTask
        member this.GetClaimsAsync(user: ApplicationUser , cancellationToken: CancellationToken ) : Task<IList<Claim>>=
            async {
                return ((user.claims |> Seq.toArray ):> System.Collections.Generic.IList<Claim>)
            } |> fun a -> Async.StartAsTask (a,cancellationToken=cancellationToken)
        member this.ReplaceClaimAsync(user: ApplicationUser, claim: Claim, newClaim: Claim, cancellationToken: CancellationToken) : Task =
            user.claims <- (user.claims |> Seq.map (fun c -> if c = claim then newClaim else claim))
            Task.CompletedTask
        member this.RemoveClaimsAsync(user: ApplicationUser, claims: IEnumerable<Claim>, cancellationToken: CancellationToken) : Task =
            user.claims <- (user.claims |> Seq.where (fun c -> not (Seq.contains c claims)))
            Task.CompletedTask
        member this.GetUsersForClaimAsync(claim: Claim, cancellationToken: CancellationToken) : Task<IList<ApplicationUser>> =
            async {
                let ret = (!users |> List.filter (fun u -> u.claims |> Seq.exists ((=) claim)) |> Seq.toArray) :> System.Collections.Generic.IList<ApplicationUser>
                return ret
            } |> fun a -> Async.StartAsTask (a,cancellationToken=cancellationToken)

    interface IUserEmailStore<ApplicationUser> with
        member this.GetEmailAsync(user:ApplicationUser, cancellationToken:CancellationToken) =
            async {
                return user.Email
            } |> fun a -> Async.StartAsTask (a,TaskCreationOptions.None,cancellationToken)
        member this.SetEmailAsync(user: ApplicationUser, email: string, cancellationToken: CancellationToken) : Task =
            user.Email<-email
            Task.CompletedTask
        member this.GetEmailConfirmedAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<bool> =
            async {
                return user.EmailConfirmed
            }|> fun a -> Async.StartAsTask (a,TaskCreationOptions.None,cancellationToken)
        member this.SetEmailConfirmedAsync(user: ApplicationUser, confirmed: bool, cancellationToken: CancellationToken) : Task =
            user.EmailConfirmed <- confirmed
            Task.CompletedTask

        member this.FindByEmailAsync(normalizedEmail: string, cancellationToken: CancellationToken) : Task<ApplicationUser> =
            this.FilterByAsync (fun i -> i.NormalizedEmail = normalizedEmail) cancellationToken

        member this.GetNormalizedEmailAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<string> =
            async {
                return user.NormalizedEmail
            } |> fun a -> Async.StartAsTask (a,TaskCreationOptions.None,cancellationToken)

        member this.SetNormalizedEmailAsync(user: ApplicationUser, normalizedEmail: string, cancellationToken: CancellationToken) : Task =
             user.NormalizedEmail <- normalizedEmail
             Task.CompletedTask


//     IUserEmailStore<ApplicationUser>, IUserPhoneNumberStore<ApplicationUser>,
//        IUserTwoFactorStore<ApplicationUser>, IUserPasswordStore<ApplicationUser>, IUserRoleStore<ApplicationUser>