namespace web
open Microsoft.AspNetCore.Identity
open System.Security.Claims
open System.Reflection
open System
// for IEndpointRouteBuilder
open Microsoft.AspNetCore.Routing
// for endpoints.Map
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Authorization
open WebSharper.AspNetCore
open FSharp.Reflection
open FSharp.Core

open System.Collections.ObjectModel


open Microsoft.Extensions.DependencyInjection

// For [Extension]
open System.Runtime.CompilerServices
open System.Runtime.InteropServices

module Routing =
    type EndpointProp ={ method: string * Http.RequestDelegate -> IEndpointConventionBuilder; path: string; policies: string list }


    let private getEndpointProps (aspnetEndpoints:IEndpointRouteBuilder) (case:UnionCaseInfo) =
        let firstArg (att:CustomAttributeData) =
            att.ConstructorArguments |> Seq.head
        // we get a CustomAttributeTypedArgument that holds a readonly collection of CustomAttributeTypedArgument whose value is a boxed string
        let getMethod  att = match (firstArg att |> fun v -> v.Value |> unbox |> (fun  (v:ReadOnlyCollection<CustomAttributeTypedArgument>) -> unbox (v.Item(0).Value)) ) with
                             | "GET" -> aspnetEndpoints.MapGet;
                             | "POST" -> aspnetEndpoints.MapPost;
                             | "PUT" -> aspnetEndpoints.MapPut;
                             | "DELETE" -> aspnetEndpoints.MapDelete;
                             | _ -> aspnetEndpoints.Map
        let getConstructorArguments (att:CustomAttributeData) =
            att.ConstructorArguments
            |> Seq.collect (fun a ->
                a.Value
                |> unbox
                |> Seq.map (fun (el:CustomAttributeTypedArgument)-> unbox (el.Value)))
        // Extracts endpoint arguments that are not put in the URL's path, ie they are posted form data or
        // placed in the query string.
        // These elements are specified in the endpoint's attribute
        let nonPathElements (case: UnionCaseInfo) =
            let attributes = case.GetCustomAttributesData()
            attributes
            |> Seq.fold (fun acc att ->
                            match att.AttributeType.Name with
                            |"FormDataAttribute"|"QueryAttribute" ->
                                let args = getConstructorArguments att
                                Seq.concat (seq[acc;args])
                            | _ ->
                                //printfn "uknown attribute %s" att.AttributeType.Name
                                acc
                        )
                        Seq.empty

        // returns elements that are place in the url's PATH by Websharper.
        let getPathElements (case:UnionCaseInfo) =
            let ignored = nonPathElements case
            let fields = case.GetFields()|> Array.map (fun e -> e.Name)
            fields
            |> Seq.fold (fun acc n ->
                    if Seq.contains n ignored then
                        acc
                    else
                        Array.concat (seq[acc;[|n|]])
                    )
                    [||]
        // returns a string specifying the matched elements in the aspnet endpoint's path
        // format: /{argname1}/{argname2}...
        // empty if none
        // this is meant to be concatenated to the websharper endpoint's path present in the attribute
        let dynamicPathElements (case:UnionCaseInfo)=
            let elements = getPathElements case
            let suffix= elements
                        |> Array.fold (fun acc e ->
                               acc+"/{"+e+"}"
                            )
                            ""
            suffix
        // iterates over all CustomAttributeData and accumulates info in the record r
        let rec getEndpointPropsInternal (atts:CustomAttributeData seq) (r:EndpointProp)=
            match atts|>List.ofSeq with
                | [] -> r
                | att::t ->
                    match att.AttributeType.Name with
                    | "EndPointAttribute" ->
                        // this holds the path
                        let suffix = (dynamicPathElements case)
                        getEndpointPropsInternal t {r with path = (sprintf "%s%s" ((firstArg att|> string |> fun s -> s.Trim('"')).Trim('/')) suffix ) }
                    | "MethodAttribute" ->
                        // this holds the HTTP method for the endpoint
                        // in the record we set the corresponding endpoint.Map${Method}
                        getEndpointPropsInternal t {r with method = (getMethod att) }
                    | "FormDataAttribute" -> r
                    | "AuthorizeAttribute" ->
                        // here we handle the policies
                        let args = att.ConstructorArguments
                        match args|>Seq.length with
                        // Authorize without arguments is requiring authentication
                        | 0 -> { r with policies="Authenticate"::r.policies}
                        // policies listed are all required, there's no further flexibility here
                        | _ -> { r with policies=(List.concat [| r.policies;
                                                                 (args
                                                                 |> Seq.toList
                                                                 |> List.map (fun a -> a.ToString().Trim('"'))
                                                                 )
                                                              |])}
                    | _ ->
                        //printfn "Unknown Attribute: %s with args %A" att.AttributeType.Name att.ConstructorArguments
                        r
        let attributes = case.GetCustomAttributesData()
        getEndpointPropsInternal attributes {method= aspnetEndpoints.MapGet; path=""; policies=[] }



    type EndpointKind = |Remoting |Page
    let MapWebSharperEndpointsInternal<'T>(aspnetEndpoints:IEndpointRouteBuilder) (kind:EndpointKind)=

        let ws = aspnetEndpoints.CreateApplicationBuilder()
                                    .UseWebSharper( fun builder -> builder.SiteletAssembly(Assembly.GetExecutingAssembly()) |> ignore)
                                    .Build()
        let endpointsType = typeof<'T>
        let duCases =
            Reflection.FSharpType.GetUnionCases endpointsType
        duCases |>  Array.iter (fun c ->
                                    let props = getEndpointProps aspnetEndpoints c
                                    let builder = match kind with
                                                  | Page -> props.method(props.path,ws)
                                                  | Remoting ->
                                                    aspnetEndpoints.MapPost(props.path,ws)
                                    props.policies
                                    |> List.iter (fun p -> builder.RequireAuthorization(props.policies|>List.toArray) |> ignore)
        )

    let MapWebSharperEndpoints<'T>(aspnetEndpoints:IEndpointRouteBuilder) =
        MapWebSharperEndpointsInternal<'T> aspnetEndpoints Page
    let MapWebSharperRemotingEndpoints<'T>(aspnetEndpoints:IEndpointRouteBuilder) =
        MapWebSharperEndpointsInternal<'T> aspnetEndpoints Remoting

[<Extension>]
type ApplicationBuilderExtensions =
    [<Extension>]
    static member UseWebSharperEndpointRouting<'T>
        (
            this: IApplicationBuilder
        ) =
        // POST requests with x-websharper-rpc are handled here
        // this means that non-POST requests with that header are not sent to websharper
        this.MapWhen( (fun httpContext -> httpContext.Request.Headers.ContainsKey("x-websharper-rpc") && httpContext.Request.Method="POST") ,
                      (fun (app:IApplicationBuilder)-> app
                                                        .UseRouting()
                                                        .UseAuthentication()
                                                        .UseAuthorization()
                                                        .UseEndpoints(fun endpoints ->
                                                                            Routing.MapWebSharperRemotingEndpoints<'T>(endpoints))
                                                                            |> ignore
                      )
            )
        // requests without the x-websharper-rpc header are handled here.
            .MapWhen( (fun httpContext -> not (httpContext.Request.Headers.ContainsKey("x-websharper-rpc"))) ,
                      (fun (app:IApplicationBuilder)-> app
                                                        .UseRouting()
                                                        .UseAuthentication()
                                                        .UseAuthorization()
                                                        .UseEndpoints(fun endpoints ->
                                                                            Routing.MapWebSharperEndpoints<'T>(endpoints))
                                                                            |> ignore
                      )
            )
