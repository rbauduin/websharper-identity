module Tests

open System
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Mvc.Testing
open Xunit
open FsUnit.Xunit
// for HttpStatusCode
open System.Net
// For WebUtility.UrlEncode
open System.Net
//for HttpClient
open System.Net.Http
open AngleSharp.Html.Dom
// for task CE from Ply
open FSharp.Control.Tasks

// for AddDefaultTokenProviders()
open Microsoft.AspNetCore.Identity

open Microsoft.AspNetCore.Authentication
open System.Security.Claims
// for IWebHostBuilder.ConfigureTestServices
open Microsoft.AspNetCore.TestHost
//for services.AddAuthentication
open Microsoft.Extensions.DependencyInjection

type TestAuthHandler(options, logger, encoder, clock) =
    inherit AuthenticationHandler<AuthenticationSchemeOptions>(options, logger, encoder, clock)

    // see https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-5.0#mock-authentication
    override __.HandleAuthenticateAsync() =
        task {
            // these are the claims we will assign to the identity we will use
            let claims =
                [ Claim(ClaimTypes.Name, "Test user")
                  Claim("level", "2") ]
            // build authentication values
            let identity = ClaimsIdentity(claims, "Test")
            let principal = ClaimsPrincipal(identity)
            let ticket = AuthenticationTicket(principal, "Test")
            // we mock a successful authentication
            let result = AuthenticateResult.Success(ticket)
            return result
        }


type BasicTest(factory: WebApplicationFactory<web.Startup>) =
    let _factory = factory
    interface IClassFixture<WebApplicationFactory<web.Startup>>

    [<Fact>]
    member __.``Get Home``() =
        task {
            let client =
                _factory.CreateClient(WebApplicationFactoryClientOptions())

            let! response = client.GetAsync("/")
            response.EnsureSuccessStatusCode() |> ignore
        }

    [<Fact>]
    member __.``Unautenticated Access protected page``() =
        task {
            let client =
                _factory.CreateClient(WebApplicationFactoryClientOptions(AllowAutoRedirect = false))

            let! response = client.GetAsync("/protected")
            printfn "response: %A" response
            Assert.Equal(HttpStatusCode.Redirect, response.StatusCode)
            Assert.Equal("?ReturnUrl=" + WebUtility.UrlEncode("/protected"), response.Headers.Location.Query)
        }

    [<Fact>]
    member __.``Autenticated Access protected page``() =
        task {
            let client =
                _factory
                    .WithWebHostBuilder(fun builder ->
                        // we will inject other services so we don't rely on those used in production
                        builder.ConfigureTestServices
                            (fun services ->
                                // in this case we can use the same user store
                                services.AddSingleton<IUserStore<web.ApplicationUser>, web.UserStore>()
                                |> ignore

                                // however we use our custom authentication handler defined above
                                // this authentication handler gives a successful result, but other handlers
                                // could be defined for failure scenarios.
                                services
                                    .AddAuthentication("Test")
                                    .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", (fun _ -> ()))
                                |> ignore

                                // inject Identity Core
                                services
                                    .AddIdentityCore<web.ApplicationUser>(fun options ->
                                        options.User.RequireUniqueEmail <- true
                                        options.SignIn.RequireConfirmedEmail <- true)
                                    .AddDefaultTokenProviders()
                                    // need to add signInManager as AddIdentityCore doesn't do it itself
                                    .AddSignInManager<SignInManager<web.ApplicationUser>>()
                                    // needed to create a user (and setting the pasword hash value)
                                    // for some reason we need to explicitly pass the type parameter, though
                                    // fantomas will remove it
                                    .AddUserManager<UserManager<web.ApplicationUser>>()
                                |> ignore


                                )
                        |> ignore)
                    .CreateClient(WebApplicationFactoryClientOptions(AllowAutoRedirect = false))

            client.DefaultRequestHeaders.Authorization <- System.Net.Http.Headers.AuthenticationHeaderValue("Test")

            let! response = client.GetAsync("/protected")
            Assert.Equal(HttpStatusCode.OK, response.StatusCode)
        }
