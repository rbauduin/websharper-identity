//these are similar to C# using statements
open canopy.runner.classic
open canopy.configuration
open canopy.classic
open canopy.types
open Microsoft.AspNetCore.Mvc.Testing
open System.Threading

canopy.configuration.chromeDir <- System.AppContext.BaseDirectory

//start an instance of chrome
start ChromeHeadless

//let factory = new WebApplicationFactory<web.Startup>()
let source = new CancellationTokenSource()
let token = source.Token

let server =
    web.Program.BuildWebHost([||]).StartAsync(token)

let setup_accounts _ =
    //go to url of locally running server
    url "http://localhost:5000"

    // check a specific element's content
    (element "/html/body/div[1]/h1").Text
    == "Say Hi to the server!"

    // click register
    click "/html/body/nav/div/ul[1]/li[6]/a"

    // then click the link to validate the email, that would be sent by mail
    click (elementWithText "a" "link to validate admin@example.com")

    // Assert the content of an element
    (read "/html/body/div[1]/p[2]")
    == "User admin@example.com is now validated: true"

    // go back to validate second account
    navigate back
    click (elementWithText "a" "link to validate admin2@example.com")

    (read "/html/body/div[1]/p[2]")
    == "User admin2@example.com is now validated: true"

let login_and_protected_pages _ =
    //go to url of locally running server
    url "http://localhost:5000"
    // click login link
    // we can either identify the element:
    // click "/html/body/nav/div/ul[1]/li[3]/a"
    // or specify its link text, but should be unique then
    click (elementWithText "a" "Login")

    // default login with empty form
    click ".btn"

    // we can check text presence in element
    contains "loggin succeeded:true" (read "/html/body/div[1]/p[3]")
    // and in whole page
    contains "loggin succeeded:true" (read "/html/body")

    // click protected2
    click "/html/body/nav/div/ul[1]/li[5]/a"
    // check it is not allowed
    contains "Forbidden" (element "/html/body").Text

    // click protected2
    click "/html/body/nav/div/ul[1]/li[4]/a"
    // check it is not allowed
    contains "is authenticated" ((element "/html/body").Text)

    // logout
    click (elementWithText "a" "Logout admin@example.com")

    // login as admin2
    click (elementWithText "a" "Login")

    "/html/body/div[1]/div/div/form/div[1]/input"
    << "admin2@example.com"

    click ".btn"
    contains "SUCCEEDED" (element "/html/body").Text

    // click protected2
    click "/html/body/nav/div/ul[1]/li[5]/a"
    contains " is now accessing this page" (element "/html/body").Text

    // logout
    click (elementWithText "a" "Logout admin2@example.com")

let reset_password _ =
    //go to url of locally running server
    url "http://localhost:5000"
    // logout
    click (elementWithText "a" "Forgot passwd")
    click (elementWithText "a" "reset pass admin@example.com")

    let newPassword = "MyNewAndLongPassword123&@#"

    "/html/body/div[1]/div/div/form/div/input"
    << newPassword

    click (elementWithText "button" "Reset Password")
    contains "Password was reset" (element "/html/body").Text
    click (elementWithText "a" "Login")
    // first check the old default password is not accepted anymore
    click ".btn"
    contains "failed :-(" (element "/html/body").Text
    navigate back

    "/html/body/div[1]/div/div/form/div[1]/input"
    << "admin@example.com"

    "/html/body/div[1]/div/div/form/div[2]/input"
    << newPassword

    click ".btn"

    contains "SUCCEEDED (rememberMe:None)" (element "/html/body").Text
    // go back to homepage. The intermediary previous page would not be displayed in
    // production, but would redirect
    // In our tests we don't redirect, so the menu entry to log out is not displayed.
    // We thus first need to go to the homepage, simulating the redirect
    url "http://localhost:5000"
    click (elementWithText "a" "Logout admin@example.com")

let require_login_on_protected_pages _ =
    //go to url of locally running server
    url "http://localhost:5000/protected"
    on "http://localhost:5000/login?ReturnUrl=%2Fprotected"

//this is how you define a test
"Setup accounts" &&& setup_accounts
// from here we can use the accounts

"Logging in and page protections"
&&& login_and_protected_pages

"Redirect to login page for unauthenticated access to protected pages"
&&& require_login_on_protected_pages

"Reset password and login" &&& reset_password

//System.Console.ReadLine() |> ignore

//run all tests
run ()

source.Cancel()

quit ()
