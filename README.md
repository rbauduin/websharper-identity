# Use ASP.Net Endpoint Routing and Identity Core with WebSharper

This is a small demo app using ASP.Net Endpoint routing with WebSharper, authentication and authorization being handled by Identity Core.

It features:
* the web app, defined in the project `websharper-identity.fsproj`, and with its code in the root directory
* unit tests, under `unit-tests/`
* selenium tests, under `selenium-tests`

Regarding the use of Identity Core, the files of interest are
* `Identity/UserStore.fr`: code to glue Identity Code with your user database. Implement interfaces as required to store uders, claims, emails, ...
* `Identity/ApplicationUser.fs`: holds the `ApplicationUser` class derived from `IdentityUser`. We store the claims assigned to a user in this class.
* `Identity/WSHelper.fs`: contains the `ApplicationBuilderExtension` and its helper functions. This enables to configure endpoint routing by a single call to `.UseWebSharperEndpointRouting<EndPoint>()` in `Configure` of the ASPNet Core's `Statup` class (`EndPoint` being the type defining the websharper endpoints, see `Site.fs`).
* `Site.fs` holds the endpoint definitions, including ASPNet Attributes like `Authorize` and `Authorize ${claim}` configuring the required claims to access the endpoint.
* `Startup.fs`, holding the ASPnet Core`Startup` class, in which the Identity Core services are configured


# Quirks
Those happen when the RPC call is manually modified:
* an unauthenticated rpc call to an endpoint requiring authentication will redirect to the login page
* an manually crafted rpc call send to a websharper endpoint will be routed to websharper, but if websharper considers it invalid it will not be handled, leading to a 500 status code with the error mentioning the endpoint the request was routed to didn't generate a response.
