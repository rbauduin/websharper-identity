namespace web

open WebSharper
open WebSharper.Sitelets
open WebSharper.UI
open WebSharper.UI.Server
open WebSharper.AspNetCore
open Microsoft.AspNetCore.Authorization;
open Microsoft.AspNetCore.Identity;
open System.Security.Claims
open Microsoft.Extensions.DependencyInjection
open Routing

type EndPoint =
    | [<EndPoint "/">] Home
    | [<EndPoint "/about">] About
    | [<EndPoint "/login"; Query("returnUrl")>] Login of returnUrl: string option
    | [<EndPoint "/protected"; Authorize>] Protected
    | [<EndPoint "/protected2";Authorize "Level2">] Protected2
    | [<EndPoint "/authenticate"; Method "POST"; FormData("login","password","returnUrl","rememberMe")>] Authenticate of login:string * password:string * returnUrl:string * rememberMe:string option
    | [<EndPoint "/register">] Register
    | [<EndPoint "/validate">] Validate of email:string * token:string
    | [<EndPoint "/forgot_password">] ForgotPassword
    | [<EndPoint "/reset_password">] ResetPassword of email:string * token:string
    | [<EndPoint "/set_password"; Method "POST"; FormData("email","token","password")>] SetPassword of email:string * token:string * password:string
    | [<EndPoint "/logout">] Logout
    | [<EndPoint "/forbidden">] Forbidden

module Templating =
    open WebSharper.UI.Html

    type MainTemplate = Templating.Template<"templates/Main.html">

    // Compute a menubar where the menu item for the given endpoint is active
    let MenuBar (ctx: Context<EndPoint>) endpoint : Doc list =
        let ( => ) txt act =
             // attr.``class`` takes as argument the string listing all classes to be applied to element
             li  [ attr.``class`` ("nav-item" + (if endpoint = act then  " active" else "")) ]
                       [
                            a [attr.href (ctx.Link act)
                               attr.``class`` "nav-link"]
                               [text txt]
                       ]
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        // comment out for tests, as it didn't find it. not problem as not used here
        //let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let userHttp = httpContext.User.Identity.Name
        // only append logout if user is logged in. Ugly code, I know
        List.append [
            "Home" => EndPoint.Home
            "About" => EndPoint.About
            "Login" => EndPoint.Login None
            "Protected" => EndPoint.Protected
            "Protected2" => EndPoint.Protected2
            "Register" => EndPoint.Register
         ] (if httpContext.User.Identity.IsAuthenticated then
                [(sprintf "Logout %s" userHttp) => EndPoint.Logout]
            else
                ["Forgot passwd" => EndPoint.ForgotPassword]
            )
    let Main ctx action (title: string) (body: Doc list) =
        Content.Page(
            MainTemplate()
                .Title(title)
                .MenuBar(MenuBar ctx action)
                .Body(body)
                .Doc()
        )

module Site =
    open WebSharper.UI.Html

    type private loginTemplate = Templating.Template<"templates/Page.Login.html">
    type private resetPasswordTemplate = Templating.Template<"templates/Page.ResetPassword.html">

    let HomePage ctx =
        Templating.Main ctx EndPoint.Home "Home" [
            h1 [] [text "Say Hi to the server!"]
            div [] [client <@ Client.Main() @>]
        ]

    let AboutPage ctx =
        Templating.Main ctx EndPoint.About "About" [
            h1 [] [text "About"]
            p [] [text "This is a template WebSharper client-server application."]
        ]

    let LoginPage ctx (returnUrl:string option)=
        Templating.Main ctx EndPoint.About "About" [
            h1 [] [text "Login"]
            div [] [ loginTemplate().ReturnUrl(returnUrl|> Option.orElse (Some "/")|> Option.get).Doc() ]
        ]

    let ProtectedPage (ctx:Context<EndPoint>)=
        let httpContext = ctx.HttpContext()
        // ^ is shotcut for the next line:
        // let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = httpContext.RequestServices.GetService<SignInManager<ApplicationUser>>()
        async {
            let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
            let userHttp = httpContext.User.Identity.Name
            return! Templating.Main ctx EndPoint.About "Protected" [
                h1 [] [text "Protected"]
                p [] [text "This is a protected page that will require a logged in user"]
                p [] [text (sprintf "The user (from signin manager:%A, from httpcontext:%A) is authenticated" user userHttp)]
                p [] [ text (sprintf "user from httpContext has claims %A" (httpContext.User.Claims |> Seq.map (sprintf "%A"))) ]
            ]
        }

    let Protected2Page (ctx:Context<EndPoint>)=
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        async {
            let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
            let userHttp = httpContext.User.Identity.Name
            return! Templating.Main ctx EndPoint.About "Protected2" [
                h1 [] [text "Protected2"]
                p [] [text "This is a protected page that will require a level2 claim!"]
                p [] [text (sprintf "The user (from signin manager:%A, from httpcontext:%A) is now accessing this page" user userHttp)]
            ]
        }

    let RegisterPage (ctx:Context<EndPoint>) =
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let store = unbox (serviceProvider.GetService(typeof<IUserStore<ApplicationUser>>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let userManager:Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>>))
        let login = "admin@example.com"
        let password = "Admin123&"
        let u=ApplicationUser()
        u.UserName<-login
        u.Email<-login

        let login2 = "admin2@example.com"
        let u2=ApplicationUser()
        u2.UserName<-login2
        u2.Email<-login2
        async {
            let! result = userManager.CreateAsync(u,password) |> Async.AwaitTask
            let! token1 = (userManager.GenerateEmailConfirmationTokenAsync(u)|> Async.AwaitTask)
            let! claim2Result = userManager.AddClaimAsync(u2,Claim("level","2"))|>Async.AwaitTask
            let! result2 = userManager.CreateAsync(u2,password) |> Async.AwaitTask
            let! token2 = (userManager.GenerateEmailConfirmationTokenAsync(u2)|> Async.AwaitTask)
            return! Templating.Main ctx EndPoint.About "Register Action" [
                h1 [] [text "Register action"]
                p [] [text (sprintf "This action register a user %s/%s" login password)]
                p [] [(if (result.Succeeded) then (a [attr.href (ctx.Link (Validate(u.Email,token1)))] [text "link to validate admin@example.com"]) else result.Errors|> Seq.map(fun e -> e.Description) |> String.concat "-"|> text )]
                p [] [text (sprintf "This action register a user %s/%s" login2 password)]
                p [] [text (if (claim2Result.Succeeded) then "admin2 claim SUCCEEDED" else claim2Result.Errors|> Seq.map(fun e -> e.Description) |> String.concat "-" )]
                p [] [(if (result2.Succeeded) then (a [attr.href (ctx.Link (Validate(u2.Email,token2)))] [text "link to validate admin2@example.com"]) else result2.Errors|> Seq.map(fun e -> e.Description) |> String.concat "-"|> text )]
            ]
        }

    let ValidatePage (ctx:Context<EndPoint>) (email:string) (token:string)=
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let store = unbox (serviceProvider.GetService(typeof<IUserStore<ApplicationUser>>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let userManager:Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>>))
        async {
            let! user = userManager.FindByEmailAsync(email) |> Async.AwaitTask
            // TODO , handle case when user is null
            let! result = userManager.ConfirmEmailAsync(user,token) |> Async.AwaitTask
            let! confirmed = userManager.IsEmailConfirmedAsync(user) |> Async.AwaitTask
            return! Templating.Main ctx EndPoint.About "Email Validation Action" [
                h1 [] [text "Validation action"]
                p [] [text (sprintf "This action validates a user's email")]
                p [] [text (if (result.Succeeded) then (sprintf "User %s is now validated: %b" user.Email confirmed ) else result.Errors|> Seq.map(fun e -> e.Description) |> String.concat "-" )]
            ]
        }

    let AuthenticatePage (ctx:Context<EndPoint>) (_login:string)(_password:string)(returnUrl:string)(rememberMe:string option)=
        // FOR DEV: hardcode info!!!
        let login = if _login = "" then "admin@example.com" else _login
        let password= if _password = "" then "Admin123&" else _password
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let store = unbox (serviceProvider.GetService(typeof<IUserStore<ApplicationUser>>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let userManager:Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>>))
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        async {
            let! result = signInManager.PasswordSignInAsync(login, password, rememberMe.IsSome, false) |> Async.AwaitTask
            // the httpContext.User does not have the claims here, as that was set before the signin
            // to get the claims, get the ClaimsPrincipal of the user through the signInManager
            let! claimMessage = if result.Succeeded && login = "admin2" then
                                 async {
                                   let! user = userManager.FindByNameAsync(login) |> Async.AwaitTask
                                   let! claimsPrincipal = signInManager.CreateUserPrincipalAsync(user) |> Async.AwaitTask
                                   return (sprintf "login2 authenticated with claims from claimsPrincipal: %A --**-- " (claimsPrincipal.Claims |> Seq.map (sprintf "%A")))
                                        + (sprintf "claims from httpContext user (empty, normal): %A" (httpContext.User.Claims |> Seq.map (sprintf "%A")))
                                 }
                                else
                                  async {
                                    return (sprintf "no claim to assign (loggin succeeded:%A)" result.Succeeded)
                                  }
            return! Templating.Main ctx EndPoint.About "Authenticate Action" [
                h1 [] [text "Authenticate action"]
                p [] [text "This action should authenticate and setup the sesstion"]
                p [] [(if (result.Succeeded) then (a [attr.href returnUrl] [text (sprintf "SUCCEEDED (rememberMe:%A)" rememberMe)]) else text "failed :-(")]
                p [] [text claimMessage]
                p [] [text (if (result.IsLockedOut) then "LOCKED OUT" else "")]
                p [] [text (if (result.IsNotAllowed) then "NOT ALLOWED" else "")]
            ]
        }

    let ForgotPasswordPage (ctx:Context<EndPoint>)=
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let store = unbox (serviceProvider.GetService(typeof<IUserStore<ApplicationUser>>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let userManager:Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>>))
        async {
            let! user1 = userManager.FindByEmailAsync("admin@example.com") |> Async.AwaitTask
            let! token1 = userManager.GeneratePasswordResetTokenAsync(user1) |> Async.AwaitTask
            let! user2 = userManager.FindByEmailAsync("admin2@example.com") |> Async.AwaitTask
            let! token2 = userManager.GeneratePasswordResetTokenAsync(user2) |> Async.AwaitTask
            return! Templating.Main ctx EndPoint.ForgotPassword "Forgot Password Action" [
                h1 [] [text "Forgot Password"]
                p [] [text (sprintf "Password reset links, would be mailed:")]
                p [] [a [attr.href (ctx.Link (ResetPassword ("admin@example.com",token1)))] [text "reset pass admin@example.com"] ]
                p [] [a [attr.href (ctx.Link (ResetPassword ("admin2@example.com",token2)))] [text "reset pass admin2@example.com"] ]
            ]
        }

    let ResetPasswordPage (ctx:Context<EndPoint>) email token=
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let store = unbox (serviceProvider.GetService(typeof<IUserStore<ApplicationUser>>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let userManager:Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>>))
        async {
            return! Templating.Main ctx (EndPoint.ResetPassword(email,token)) "Reset Password Action" [
                h1 [] [text "Reset Password"]
                p [] [text (sprintf "Password reset form:")]
                div [] [resetPasswordTemplate().Action(ctx.Link (SetPassword("","",""))).Email(email).Token(token).Doc()]
            ]
        }

    let SetPasswordPage (ctx:Context<EndPoint>) email token password=
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let store = unbox (serviceProvider.GetService(typeof<IUserStore<ApplicationUser>>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let userManager:Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>>))
        async {
            let! user = userManager.FindByEmailAsync(email) |> Async.AwaitTask
            let! result = userManager.ResetPasswordAsync(user,token,password) |> Async.AwaitTask
            return! Templating.Main ctx (EndPoint.SetPassword(email,token,password)) "Set Password Action" [
                h1 [] [text "Set Password Result"]
                p [] [(if (result.Succeeded) then (text "Password was reset") else result.Errors|> Seq.map(fun e -> e.Description) |> String.concat "-"|> text )]
            ]
        }

    let LogoutPage (ctx:Context<EndPoint>)=
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let store = unbox (serviceProvider.GetService(typeof<IUserStore<ApplicationUser>>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let userManager:Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.UserManager<ApplicationUser>>))
        //signinManager.CheckPasswordSignInAsync()
        async {
            let! result = signInManager.SignOutAsync()|> Async.AwaitTask
            return! Templating.Main ctx EndPoint.Logout "Logout" [
                h1 [] [text "Logout action"]
                p [] [text "This action should logout the user"]
            ]
        }

    let ForbiddenPage (ctx:Context<EndPoint>)=
        Templating.Main ctx EndPoint.Logout "Forbidden" [
            h1 [] [text "Forbidden"]
            p [] [text "You don't have access to the page you requested"]
        ]
        |> Content.SetStatus Http.Status.Forbidden


    [<Website>]
    let Main =
        Application.MultiPage (fun ctx endpoint ->
            match endpoint with
            | EndPoint.Home -> HomePage ctx
            | EndPoint.About -> AboutPage ctx
            | EndPoint.Login returnUrl-> LoginPage ctx returnUrl
            | EndPoint.Protected -> ProtectedPage ctx
            | EndPoint.Protected2 -> Protected2Page ctx
            | EndPoint.Authenticate (login,password,returnUrl, rememberMe) -> AuthenticatePage ctx login password returnUrl rememberMe
            | EndPoint.Register -> RegisterPage ctx
            | EndPoint.Validate (email,token) -> ValidatePage ctx email token
            | EndPoint.ForgotPassword -> ForgotPasswordPage ctx
            | EndPoint.ResetPassword (email,token) -> ResetPasswordPage ctx email token
            | EndPoint.SetPassword (email,token,password) -> SetPasswordPage ctx email token password
            | EndPoint.Logout -> LogoutPage ctx
            | EndPoint.Forbidden -> ForbiddenPage ctx
        )
