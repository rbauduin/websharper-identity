namespace Test
module tests=

    open NUnit.Framework
    open FSharp.Control.Tasks
    open System.Threading

    let source = new CancellationTokenSource()
    let token = source.Token
    [<SetUp>]
    let Setup () =
        ()

    [<TestFixture;Parallelizable>]
    type MyTest() =
        [<OneTimeSetUp>]
        member _.InitializeAsync() = unitTask {
            System.IO.File.WriteAllText("/tmp/witness", "will start WS app")
            let builder = web.Program.WebHostBuilder([||])
            return builder.Build().StartAsync(token)
        }
        [<OneTimeTearDown>]
        member __.DisposeAsync() = unitTask {

            source.Cancel()
            return ()
        }

        [<Test>]
        member self.``first_test`` () =
            System.Console.ReadLine() |> ignore
            Assert.Pass()
