namespace Tests

open System
open NUnit
open NUnit.Framework
open FSharp.Control.Tasks
open System.Threading

module Database =

    [<SetUpFixture>]
    type DatabaseFixture()=

        let source = new CancellationTokenSource()
        let token = source.Token

        [<OneTimeSetUp>]
        member _.InitializeAsync() = unitTask {
            System.IO.File.WriteAllText("/tmp/witness", "will start WS app")
            let builder = FromTemplate.Program.WebHostBuilder([||])
            return builder.Build().StartAsync(token)
        }
        [<OneTimeTearDown>]
        member __.DisposeAsync() = unitTask {

            source.Cancel()
            return ()
        }
